from tkinter import ttk
import tkinter as tk
from tkinter import messagebox
import random

class Player(ttk.PanedWindow):
    
    def __init__(self, master, rows, cols, name):
        
        ttk.Frame.__init__(self, master)

        # VARIABLES
        self.name = name
        self.font = ("Helvetica", 16) # https://effbot.org/tkinterbook/label.htm
        self.rows = rows
        self.cols = cols
        self.percentage = 0.2
        self.material = round(self.rows*self.cols*self.percentage)
        self.player_ships = list()
        self.occupied_coord = list()
        self.ready = False
        self.next_player = False
        self.end_game = False
        
        # PLAYER'S BOARD
        left_frame = tk.Frame(self)
        tk.Label(self, text="Player").grid(row=0, column=0)
        left_frame.grid(row=1, column=0)
        self.left_grid = self.create_left_grid(left_frame)

        # SEPARATOR to distinguish between boards
        separator = tk.Frame(self, width=10)
        separator.grid(row=0, column=1)

        # ENEMY'S BOARD
        right_frame = tk.Frame(self)
        tk.Label(self, text="Enemy").grid(row=0, column=2)
        right_frame.grid(row=1, column=2)
        self.right_grid = self.create_right_grid(right_frame)

        # CONTROL PANEL
        control_panel = tk.Frame(self)
        control_panel.grid(row=1, column=3)
        
        sub_panel1 = tk.Frame(control_panel)
        sub_panel1.grid(row=0, column=0)

        tk.Label(sub_panel1, text="Remaining cells", bg='Green', activebackground='red', font=self.font).grid(row=2, column=0, sticky='WE')
        self.space_l = tk.Label(sub_panel1, text="{0}".format(self.material), bg='Green', activebackground='red', font=self.font)
        self.space_l.grid(row=3, column=0, sticky='WE')

        # Auto-mode places all ships except the first one and sends 'ready'-signal automatically
        self.auto_b = tk.Button(sub_panel1, text='Auto', bg='Green', activebackground='red', command=self.auto_mode, font=self.font)
        self.auto_b.grid(row=4, column=0, sticky='WE')

        sub_panel2 = tk.Frame(control_panel)
        sub_panel2.grid(row=1, column=0)

        # Ship sizes
        self.size = tk.IntVar()

        self.ships_b = dict()
        for i in range(2,6):
            tk.Label(sub_panel2, text='{0}'.format(i+1)).grid(row=i, column=0, sticky='W')
            self.ships_b[i] = tk.Radiobutton(sub_panel2, variable=self.size, value=i+1)     # https://www.tutorialspoint.com/python/tk_radiobutton.htm
            self.ships_b[i].grid(row=i, column=1, sticky='W')
            
        self.size.set(3)

        # Vertical/horizontal positioning
        tk.Label(sub_panel2, text='V').grid(row=6, column=0, sticky='W')
        tk.Label(sub_panel2, text='H').grid(row=7, column=0, sticky='W')
        self.p = tk.StringVar()
        self.p.set('V')
        self.pos = {'V': tk.Radiobutton(sub_panel2, variable=self.p, value='V'),
                    'H': tk.Radiobutton(sub_panel2, variable=self.p, value='H')}
        self.pos['V'].grid(row=6, column=1, sticky='W')
        self.pos['H'].grid(row=7, column=1, sticky='W')

    def auto_mode(self):
        
        self.auto_b.configure(state='disabled')

        # At least one cell must be occupied before auto-mode could work
        if not(1 in list(self.left_grid_mask.values())):
            messagebox.showinfo("Warning!", "Place at least one ship first")
            self.auto_b.configure(state='normal')
            return
        
        while self.material >= 3:
            
            random_size = random.choice([3, 4, 5, 6])
            self.size.set(random_size)
            
            random_direction = random.choice(['H', 'V'])
            self.p.set(random_direction)
            
            random_col = random.choice(range(self.cols))
            random_row = random.choice(range(self.rows))

            self.fill_cell("{0}:{1}".format(random_row, random_col), auto=True)
        
    # Create player-field filled with buttons according to user-input
    def create_left_grid(self, board):
        
        grid = dict()
        self.left_grid_mask = dict()
        for i in range(self.rows):
            for j in range(self.cols):
                
                grid["{0}:{1}".format(i, j)] = tk.Button(board, text="", fg='black', bg='blue', activebackground='green',
                                                     command=lambda argx=i,argy=j:self.fill_cell("{0}:{1}".format(argx, argy))) # https://cmsdk.com/python/python-tkinter-how-to-change-color-of-button-on-press.html
                grid["{0}:{1}".format(i, j)].grid(row=i, column=j, sticky='W')
                self.left_grid_mask["{0}:{1}".format(i, j)] = 0
                
        return grid
    
    # Create enemy-field filled with buttons according to user-input
    def create_right_grid(self, board):
        
        grid = dict()
        self.right_grid_mask = dict()
        for i in range(self.rows):
            for j in range(self.cols):
                grid["{0}:{1}".format(i, j)] = tk.Button(board, text="", fg='black', bg='blue', activebackground='green', state="disabled",
                                                         command=lambda argx=i,argy=j:self.shoot("{0}:{1}".format(argx, argy))) # https://www.tutorialspoint.com/python/tk_button.htm
                grid["{0}:{1}".format(i, j)].grid(row=i, column=j, sticky='W')
                self.right_grid_mask["{0}:{1}".format(i, j)] = 0

        return grid

    def shoot(self, rowcol):

        if self.right_grid_mask[rowcol] == 1:
            self.right_grid_mask[rowcol] = '?'
            self.right_grid[rowcol].configure(bg='#ff9900')
            self.right_grid[rowcol].configure(state='disabled')
            for ships in self.enemy_ships:
                if rowcol in ships:
                    cells = ships.split('-')
                    ship_length = len(cells)
                    damage = 0
                    for cell in cells:
                        if self.right_grid_mask[cell] == '?':
                            damage += 1
                    if damage == ship_length:
                        for cell in cells:
                            self.right_grid_mask[cell] ='X'
                            self.right_grid[cell].configure(bg='red')
                            self.right_grid[cell].configure(state='disabled')
                        if not(1 in list(self.right_grid_mask.values())):
                            self.next_player = True
                            messagebox.showinfo('Game is over',"{0} is the winner!".format(self.name))
        else:
            self.right_grid_mask[rowcol] = '.'
            self.right_grid[rowcol].configure(bg='white')
            self.right_grid[rowcol].configure(state='disabled')
            for i in range(self.rows):
                for j in range(self.cols):
                    self.right_grid["{0}:{1}".format(i, j)].configure(state='disabled')
            messagebox.showinfo("Missed!", "Next player must shoot")
            self.next_player = True

    def build_fleet(self, rowcol, flag, auto=False):
        
        length = int(self.size.get())

        if self.material - length < 0:
            if not auto:
                messagebox.showinfo('Warning!', "Not enough cells")
            self.left_grid_mask[rowcol] = 0
            self.left_grid[rowcol].configure(bg="blue")
            return

        if self.material < 3 and self.material - length < 0:
            self.left_grid_mask[rowcol] = 0
            self.left_grid[rowcol].configure(bg="blue")
            for i in range(self.rows):
                for j in range(self.cols):
                    self.left_grid['{0}:{1}'.format(i,j)].configure(state='disabled')
            self.auto_b.configure(state='disabled')
            self.ready = True
            return

        row = int(rowcol.split(':')[0])
        column = int(rowcol.split(':')[1])
        
        self.left_grid_mask[rowcol] = flag

        if auto:
            res = self.place_ship(length, row, column, False)
        else:
            res = self.place_ship(length, row, column)
            
        if res:
            self.material -= length
            self.space_l['text'] = "{0}".format(self.material)

        if self.material < 3:
            for i in range(self.rows):
                for j in range(self.cols):
                    self.left_grid['{0}:{1}'.format(i,j)].configure(state='disabled')
            self.auto_b.configure(state='disabled')
            self.ready = True
            return
            
    def place_ship(self, length, row, column, msg=True):
        
        new_ship = Ship(length)

        # Place ship either hotizontal or vertical
        if str(self.p.get()) == "H":
            new_ship.ship_horizontal(row, column)
        else:
            new_ship.ship_vertical(row, column)

        # Check whether coordinates of new ship overlaps with previous ones or not
        for r,c in new_ship.used_cords:
            if ((r,c) in self.occupied_coord):
                self.left_grid['{0}:{1}'.format(new_ship.used_cords[0][0], new_ship.used_cords[0][1])].configure(bg="blue")
                self.left_grid_mask['{0}:{1}'.format(new_ship.used_cords[0][0], new_ship.used_cords[0][1])] = 0
                if msg:
                    messagebox.showinfo('Warning!', "You cannot place the ship here.\nOverlapping coordinates")
                return False

        # Check whether ship goes beyond the board or not
        for r,c in new_ship.used_cords:
            if r >= self.rows or c >= self.cols:
                self.left_grid['{0}:{1}'.format(new_ship.used_cords[0][0], new_ship.used_cords[0][1])].configure(bg="blue")
                self.left_grid_mask['{0}:{1}'.format(new_ship.used_cords[0][0], new_ship.used_cords[0][1])] = 0
                if msg:
                    messagebox.showinfo('Warning!', "Ship goes beyod the board")
                return False
            
        self.player_ships.append(str(new_ship.used_cords)[2:-2].replace('), (', '-').replace(', ', ':'))
        self.occupied_coord += new_ship.used_cords

        for r,c in new_ship.used_cords:
            self.left_grid['{0}:{1}'.format(r,c)].configure(bg="green")
            self.left_grid['{0}:{1}'.format(r,c)].configure(state='disabled')
            self.left_grid_mask['{0}:{1}'.format(r,c)] = 1
            
        return True

    # Chosen cell changes its color by clicking on it
    def fill_cell(self, rowcol, auto=False):
        if self.left_grid[rowcol]['bg'] == "blue": # https://bytes.com/topic/python/answers/627614-tkinter-how-get-buttons-bg-color
            self.left_grid[rowcol].configure(bg="green")
            if auto:
                self.build_fleet(rowcol, 1, auto=True)
            else:
                self.build_fleet(rowcol, 1)

    def set_enemy_ships(self, enemy_ships):
        self.enemy_ships = enemy_ships

    def get_ships(self):
        return self.player_ships

    def get_masks(self):
        return self.left_grid_mask, self.right_grid_mask

    def set_masks(self, left_mask, right_mask):
        self.left_grid_mask, self.right_grid_mask = left_mask, right_mask

    def activate_enemy_field(self):
        for i in range(self.rows):
            for j in range(self.cols):
                self.right_grid['{0}:{1}'.format(i,j)].configure(state='normal')

    def activate_only_non_shoot_fields(self):
        for i in range(self.rows):
            for j in range(self.cols):
                if self.right_grid_mask['{0}:{1}'.format(i,j)] == 1 or self.right_grid_mask['{0}:{1}'.format(i,j)] == 0:
                    self.right_grid['{0}:{1}'.format(i,j)].configure(state='normal')
                    
    def update_players_field(self, update_mask):
        
        for i in update_mask:
            if update_mask[i] == '.':
                self.left_grid_mask[i] = '.'
                self.left_grid[i].configure(bg="white")
                self.left_grid[i].configure(state='disabled')
            elif update_mask[i] == '?':
                self.left_grid_mask[i] = '?'
                self.left_grid[i].configure(bg="#ff9900")
                self.left_grid[i].configure(state='disabled')
            elif update_mask[i] == 'X':
                self.left_grid_mask[i] = 'X'
                self.left_grid[i].configure(bg="red")
                self.left_grid[i].configure(state='disabled')
    
class Ship():
    
    def __init__(self, length):
        self.length = length
        self.used_cords = []
        
    def ship_horizontal(self, row, column):
        self.used_cords.append((row, column))
        for i in range(self.length-1):
            column += 1
            self.used_cords.append((row, column))
            
    def ship_vertical(self, row, column):
        self.used_cords.append((row, column))
        for i in range(self.length-1):
            row += 1
            self.used_cords.append((row, column))
        
class StartWindow(tk.Tk):
    
    def __init__(self):
        
        self.start_window = tk.Tk()
        self.start_window.title("Battleships")
        self.font = ("Helvetica", 16)
        
        # Labels
        tk.Label(self.start_window, text="Columns", font=self.font).grid(row=1, column=0, sticky='N')
        tk.Label(self.start_window, text="Rows", font=self.font).grid(row=2, column=0, sticky='W')
        
        # Entry for columns
        self.cols_entry = tk.Entry(self.start_window,font=self.font)
        self.cols_entry.insert(0, 10)
        self.cols_entry.grid(row=1, column=1)
        
        # Entry for rows
        self.rows_entry = tk.Entry(self.start_window,font=self.font)
        self.rows_entry.insert(0, 10)
        self.rows_entry.grid(row=2, column=1)

        # Start button
        self.start_b = tk.Button(self.start_window, text="Preparation", command=self.choose_player,font=self.font)
        self.start_b.grid(row=3, column=2)

    def choose_player(self):
        
        # Checks the inputs first
        if self.check_inputs(self.rows_entry.get(), self.cols_entry.get()):
            
            # Random choice among two options
            self.players = {'1':0, '2':2}
            self.first = random.choice(list(self.players.keys()))
            self.start_window.destroy()                
            self.start()
            
    # Inputs check. They must be of type integer otherwise throws exception with error-messagebox
    def check_inputs(self, rows, cols):

        # Check columns
        try:
            self.cols = int(cols)
        except ValueError:
            messagebox.showinfo("Wrong input", "Columns-parameter must be an integer") # https://pythonspot.com/tk-message-box/
            self.cols_entry.delete(0, last=None)
            return

        # Check rows
        try:
            self.rows = int(rows)
        except ValueError:
            messagebox.showinfo("Wrong input", "Rows-parameter must be an integer")
            self.cols_entry.delete(0, last=None)
            return

        # If both are OK return True
        return True

    def start_game(self, player1, player2):
        
        if not player1.ready or not player2.ready:
            messagebox.showinfo("False start", "Players are not yet ready")
            return
        
        self.start_b.configure(state='disabled')
        
        # Selects and notifies who goes first
        # self.nb.select(self.players[self.first])
        self.nb.select(self.shader)
        messagebox.showinfo("Preparation", "Player#{0} goes first".format(self.first))

        # Bind with callback (by changing the tab)
        self.nb.bind("<Button-1>", self.callback_notebook)
        # https://stackoverflow.com/questions/40828166/is-it-possible-to-bind-a-mouse-event-to-a-tab-of-a-notebook
        # https://effbot.org/tkinterbook/tkinter-events-and-bindings.htm
        
        player1.activate_enemy_field()
        player2.activate_enemy_field()

        player2.set_enemy_ships(player1.get_ships())
        player1.set_enemy_ships(player2.get_ships())

        self.callback_notebook('')

    def callback_notebook(self, event):
            
        (self.player1_left, self.player1_right) = self.player1.get_masks()
        (self.player2_left, self.player2_right) = self.player2.get_masks()
        
        self.player1.update_players_field(self.player2_right)
        self.player2.update_players_field(self.player1_right)
        
        self.player1.set_masks(self.player1_left, self.player2_left)
        self.player2.set_masks(self.player2_left, self.player1_left)

        if self.player1.next_player:
            self.player2.next_player = True
            self.player1.next_player = False
            self.player1.activate_only_non_shoot_fields()
            
        if self.player2.next_player:
            self.player1.next_player = True
            self.player2.next_player = False
            self.player2.activate_only_non_shoot_fields()

        print("Player2 must go - ", self.player1.next_player)
        print("Player1 must go - ", self.player2.next_player)     

    def exit_game(self, root):
        if self.nb.index(self.nb.select()) == 0: # https://stackoverflow.com/questions/14000944/finding-the-currently-selected-tab-of-ttk-notebook
            messagebox.showinfo("Game is over", "Player1 lost")
        if self.nb.index(self.nb.select()) == 2: 
            messagebox.showinfo("Game is over", "Player2 lost") 
        root.destroy()
        
    def start(self):

        root = tk.Tk()
        root.title("Battleships")

        self.nb = ttk.Notebook(root) # https://stackoverflow.com/questions/16514617/python-tkinter-notebook-widget

        # First player
        self.player1 = Player(self.nb, self.rows, self.cols, 'Player1')
        
        # Blank page to hide the fields while
        # passing the app from one player to another
        self.shader = ttk.Frame(self.nb)
        
        # Second player
        self.player2 = Player(self.nb, self.rows, self.cols, 'Player2')

        # Place players as tabs on main window
        self.nb.add(self.player1, text='Player1')
        self.nb.add(self.shader, text='Shader')
        self.nb.add(self.player2, text='Player2')
        
        # Exit button
        exit_b = tk.Button(root, text="Exit", fg='black', bg='blue', activebackground='red', command=lambda r=root: self.exit_game(r))
        exit_b.pack(side = 'top')

        self.start_b = tk.Button(root, text="Start", fg='black', bg='blue', activebackground='red', command=lambda p1=self.player1, p2=self.player2: self.start_game(p1, p2))
        self.start_b.pack(side = 'top')       
        self.nb.pack(expand=1, fill="both")
        
        # Game rules  
        # messagebox.showinfo("Battleships Game Rules", "Add the description here in the line 461")
        
        # Close-event by pressing 'X'
        root.protocol("WM_DELETE_WINDOW", root.destroy) # https://stackoverflow.com/questions/111155/how-do-i-handle-the-window-close-event-in-tkinter
        
        root.mainloop()

if __name__ == "__main__":
    sw = StartWindow()
    
